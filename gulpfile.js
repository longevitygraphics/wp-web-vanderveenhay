// ----- gulp required var
var gulp                   = require('gulp'),
    watch                  = require('gulp-watch'),
    concat                 = require('gulp-concat'),
    svgstore               = require('gulp-svgstore'),
    imagemin               = require('gulp-imagemin'),
    imageminJpegRecompress = require('imagemin-jpeg-recompress'),
    imageminPngquant       = require('imagemin-pngquant'),
    rename                 = require('gulp-rename'),
    jsmin                  = require('gulp-uglify'),
    svgmin                 = require('gulp-svgmin'),
    path                   = require('path'),
    sass                   = require('gulp-sass'),
    autoprefixer           = require('gulp-autoprefixer'),
    notify                 = require("gulp-notify"),
    plumber                = require('gulp-plumber'),
    sourcemaps             = require('gulp-sourcemaps'),
    browserSync            = require('browser-sync').create();

// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        open: "external",
        // proxy: "http://test.test/"
        proxy: "http://vanderveenhay.test/"
      });
});

// ----- Path Configurations
var config = {
    sassPath            : './assets/src/sass',
    parentSassPath      : './../wp-theme-parent/assets/src/sass',
    cssPublic           : './assets/dist/css',
    jsPath              : './assets/src/js',
    jsPublic            : './assets/dist/js',
    svgPath             : './assets/src/svg-icons',
    nodePath            : './node_modules',
    imagePublic         : './assets/dist/images',
    imagesrc            : './assets/src/images'
};

// ---- SASS Load Paths
var sassLoad = [
    config.nodePath + '/'
];

// Get Assets from node_modules
gulp.task('getassets', function() {
    // Font Awesome
    gulp.src( config.nodePath + '/@fortawesome/fontawesome-free-webfonts/webfonts/*.*' )
    .pipe(gulp.dest('./assets/dist/fonts'));

    // Slick
    gulp.src(config.nodePath + '/slick-carousel/slick/fonts/**.*')
    .pipe(gulp.dest('./assets/dist/fonts'));

    // Slick
    gulp.src(config.nodePath + '/slick-carousel/slick/ajax-loader.gif')
    .pipe(gulp.dest('./assets/dist/images'));
});

// ----- SASS
gulp.task('mainCSS', function() {
    return gulp.src([
      config.sassPath + '/style.scss'
    ])
 .pipe(sourcemaps.init())
 .pipe(sass({ outputStyle: 'compressed', includePaths: sassLoad})
 .on('error', sass.logError))
 .pipe(autoprefixer({ browsers: ['last 3 versions']}))
 .pipe(sourcemaps.write('.'))
 .pipe(gulp.dest('.'))
 .pipe(browserSync.stream());
});

// ----- SVG Store
gulp.task('svgstore', function () {
    return gulp
        .src(config.svgPath + '/**/*.svg')
        .pipe(svgmin(function (file) {
            var prefix = path.basename(file.relative, path.extname(file.relative));
            return {
                plugins: [{
                    cleanupIDs: {
                        prefix: prefix + '-',
                        minify: true
                    }
                }]
            }
        }))
        .pipe(svgstore())
        .pipe(gulp.dest(config.imagePublic));
});

// ----- Image Min
gulp.task("imagemin", function(){
    return gulp.src(config.imagesrc + '/**/*')
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imageminJpegRecompress({
                progressive: true,
                max: 80,
                min: 70
            }),
            imageminPngquant({quality: '75-85'}),
            imagemin.svgo({plugins: [{removeViewBox: false}]})
        ]))
        .pipe(gulp.dest(config.imagePublic));
});


// ----- js min
gulp.task('mainJS', function() {
    gulp.src(config.jsPath + '/**/*.js')
        .pipe(concat('script.min.js'))
        .pipe(jsmin())
        .pipe(gulp.dest(config.jsPublic));
});

// ----- Watch
gulp.task('watch', function(){
    gulp.watch(config.sassPath + '/**/*.scss', ['mainCSS']);
    gulp.watch(config.parentSassPath + '/**/*.scss', ['mainCSS']);
    gulp.watch(config.imagesrc + '/**/*', ['imagemin']);
    gulp.watch(config.jsPath + '/**/*.js', ['mainJS']);
    gulp.watch(config.svgPath + '/**/*', ['svgstore']);
});

// ----- Default
gulp.task('default', ['browser-sync', 'getassets', 'mainCSS', 'imagemin', 'mainJS', 'svgstore', 'watch' ]);