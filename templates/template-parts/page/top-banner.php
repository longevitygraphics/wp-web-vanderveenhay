<div class="top-banner text-uppercase">
	<?php 
		$top_banner_images = get_field('top_banner');
		$top_banner_images = get_field("top_banner")['gallery'];
		$banner_cta_one = get_field("banner_cta_1");
		
		if ($banner_cta_one && is_array( $banner_cta_one ) ) {
			$banner_cta_one_title = $banner_cta_one["title"];
			$banner_cta_one_link = $banner_cta_one["url"];
		}
		
		$banner_cta_two = get_field("banner_cta_2");
		
		if ($banner_cta_two && is_array( $banner_cta_two ) ) {
			$banner_cta_two_title = $banner_cta_two["title"];
			$banner_cta_two_link = $banner_cta_two["url"];
		}
		
		$gallery = 	$top_banner_images;
		$text_overlay = get_field("text_overlay");

		if($top_banner_images){
			include(locate_template('/layouts/components/gallery.php'));
		}

		if($text_overlay || $banner_cta_one || $banner_cta_two){
			echo "<div class='top-banner-overlay'>";
			if($text_overlay){
				echo "<div class='banner-text'>";
				echo $text_overlay;
				echo "</div>"; //end of banner text
			}
			if($banner_cta_one || $banner_cta_two){
				echo "<div class='banner-cta'>";
					if($banner_cta_one){
						echo "<a class='btn btn-white h3' href='";
						echo $banner_cta_one_link;
						echo "'>";
						echo $banner_cta_one_title;
						echo "</a>"; // end of btn one
					}
					if($banner_cta_two){
						echo "<a class='btn btn-primary h3' href='";
						echo esc_url($banner_cta_two_link);
						echo "'>";
						echo $banner_cta_two_title;
						echo "</a>"; // end of btn one
					}
				echo "</div>"; // end of banner cta
			}
			
			
			echo "</div>"; // end of top banner overlay
		}
	 ?>
</div>