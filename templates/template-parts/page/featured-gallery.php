<?php 

$images = get_field('gallery','option');
if( $images ): 

?>
    <div class="featured-gallery row">
        <?php foreach ($images as $key => $image): ?>

        	<div class='col-xl-3 col-md-4 col-sm-6'><img src="<?php echo $image['url']; ?>"></div>
        	<?php 
        		if($key == 7){
        			break;
        		}
        	 ?>
        <?php endforeach; ?>
    </div> <!-- end of gallery slider -->
<?php endif; ?>