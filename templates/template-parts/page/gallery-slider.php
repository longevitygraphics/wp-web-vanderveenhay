<?php 

$images = get_field('gallery','option');

if( $images ): ?>
    <div class="gallery-slider">
        <?php foreach ($images as $key => $image): ?>
        	<?php if($key % 2 == 0): ?>
        		<div>
        	<?php endif; ?>

        	<div><img src="<?php echo $image['url']; ?>"></div>

        	<?php if($key == sizeof($images) - 1): ?>
	        	</div>
	        <?php break; endif; ?>

        	<?php if($key % 2 == 1): ?>
				</div>
        	<?php endif;?>
        <?php endforeach; ?>
    </div> <!-- end of gallery slider -->
<?php endif; ?>

<script>
		(function($) {

		    $(document).ready(function(){

		    	$('.gallery-slider').slick({
					dots: true,
					autoplay: true,
		    		adaptiveHeight: true,
		    		slidesToShow: 4,
		    		slidesToScroll: 1,
		    		infinite: false,
		    		responsive: [
				    {
				      breakpoint: 1024,
				      settings: {
				        slidesToShow: 3,
				        slidesToScroll: 1,
				        infinite: true,
				        dots: true
				      }
				    },
				    {
				      breakpoint: 600,
				      settings: {
				        slidesToShow: 2,
				        slidesToScroll: 1
				      }
				    },
				    {
				      breakpoint: 480,
				      settings: {
				        slidesToShow: 1,
				        slidesToScroll: 1
				      }
				    }]
				});
		    });

		}(jQuery));
</script>