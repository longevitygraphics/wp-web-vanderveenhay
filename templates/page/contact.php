
<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package test
 */
get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="mb-5">
				<div class="mb-5 pb-5 contact-title text-center">
					<h3 class="h1 hay-title text-uppercase font-weight-bold">contact us</h3>
					<p>Want to book a delivery service or have questions about our products?</p>
					<p>Call us or fill in the form below and our team will be in touch</p>
				</div>
				<div class="contact row mx-0 px-4">
					<div class="form-section col-lg-4">
						<?php echo do_shortcode("[gravityform id=1 title=false description=false]");  ?>
					</div>
					<div class="map-section col-lg-8">
						<?php echo do_shortcode("[lg-map id=208]");  ?>
						<div class="d-lg-flex justify-content-between contact-info mt-5">
							<div class="contact-phone">
								<?php echo do_shortcode("[lg-phone-main]"); ?>
							</div>
							<div class="contact-email">
								<?php echo do_shortcode("[lg-email]"); ?>
							</div>
							<div class="contact-address">
								<?php echo do_shortcode("[lg-address] [lg-city] [lg-province] [lg-postcode]"); ?>
							</div>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();