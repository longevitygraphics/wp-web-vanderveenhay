<?php

	switch ( get_row_layout()) {
		case 'example':
			get_template_part('/layouts/layouts/example');
		break;
		case 'hay_title':
			get_template_part('/layouts/layouts/hay-title');
		break;
		case 'featured_products':
			get_template_part('/layouts/layouts/featured-products');
		break;
		case 'gallery':
			get_template_part('/layouts/layouts/gallery');
		break;
	}

?>