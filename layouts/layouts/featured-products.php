<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>


<?php 
	$args = array(
		'showposts'=> -1,
		'post_type'=> 'product'
	);

	$products = new WP_Query($args);

	if($products->have_posts()) : ?>
		<div class='product-table'>
			<div class="table-body container-fluid">
				<div class="table-head text-white">
					<div class="table-header-content">Product</div>
					<div class="table-header-content">Bale Weight</div>
					<div class="table-header-content">Price Per Bale</div>
					<div class="table-header-content">Price Per Ton</div>
					<div class="table-header-content">Bales Per Ton</div>
					<div class="table-header-content">Notes</div>
				</div>
				<div class="table-content">
				<?php while($products->have_posts()): $products->the_post(); ?>
				<?php 
					//create variables for all field
					$product_name = get_field("product_name");
					$bale_weight = get_field("bale_weight");
					$price_per_bale = get_field("price_per_bale");
					$price_per_ton = get_field("price_per_ton");
					$bales_per_ton = get_field("bales_per_ton");
					$notes = get_field("notes");
					$sold_out = get_field("sold_out");
					$this_year = get_field("this_year");
				 ?>
				 <div class="table-row">

				 	<div>
				 		<div class="product-label d-lg-none">Product</div>
				 		<div class="product-name d-flex flex-column"><?php echo "<div>" . $product_name . "</div>"; ?>
						<?php if($sold_out==1) : ?>
							<span class="d-block text-danger text-uppercase">sold out</span>
						<?php endif // end of if sold out?>
						</div>
				 	</div>

				 	<div>
				 		<div class="product-label d-lg-none">Bale Weight</div>
				 		<div><?php echo $bale_weight; ?>lbs</div>
				 	</div>

				 	<div>
				 		<div class="product-label d-lg-none">Price Per Bale</div>
				 		<div>$<?php echo $price_per_bale; ?></div>
				 	</div>

				 	<div>
				 		<div class="product-label d-lg-none">Price Per Ton</div>
						<div><?php if( $price_per_ton && is_numeric($price_per_ton) ) : ?>$<?php endif; ?><?php echo $price_per_ton; ?></div>
				 	</div>

				 	<div>
				 		<div class="product-label d-lg-none">Bales Per Ton</div>
				 		<div><?php echo $bales_per_ton; ?></div>
				 	</div>

				 	<div>
				 		<div class="product-label d-lg-none">Notes</div>
				 		<div class="d-flex flex-column"><?php echo "<div>" . $notes . "</div>"; ?>
				 		<?php if($this_year==1) : ?>
							<span class="d-block text-primary text-capitalize">this year</span>
						<?php endif // end of if this year?>
						</div>
				 	</div>
				</div>
				<?php endwhile; //end of while $products->have_post ?>
				</div>
			</div>
		</div> <!-- end of product-table -->
	<?php endif; //end of if $products->have_posts 
		  wp_reset_query();
	?>

<?php

	get_template_part('/layouts/partials/block-settings-end');

?>