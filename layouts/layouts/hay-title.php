<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<?php 

$hay_title = get_sub_field("hay_title_text");
if($hay_title) {
	echo "<div class='text-center hay-title'>";
	echo "<h3>";
	echo $hay_title;
	echo "</h3>";
	echo "</div>";
}

?>



<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
