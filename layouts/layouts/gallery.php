<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>


	
	<?php
		$gallery_layout = get_sub_field("gallery_layout");
		if($gallery_layout["value"]=="featured"){
			include(locate_template('/templates/template-parts/page/featured-gallery.php'));
		}elseif($gallery_layout["value"]=="full"){
			include(locate_template('/templates/template-parts/page/gallery-slider.php'));
		}
				 
	?>



<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>