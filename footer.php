<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
	<?php do_action('wp_content_bottom'); ?>
	</div>
	
	<?php do_action('wp_body_end'); ?>
	<?php $lg_option_footer_site_legal = get_option('lg_option_footer_site_legal'); ?>

	<footer id="site-footer">
		
		<div id="site-footer-main" class="clearfix py-3 container-fluid justify-content-center align-items-start flex-wrap">
			<div class="d-flex justify-content-between">
				<div class="site-logo">
					<img src="/wp-content/themes/wp-web-vanderveenhay/assets/dist/images/vanderveen_logo_gold.png">
				</div>
				<div class="footer-right row justify-content-md-between">
					<div class="footer-menu d-none d-lg-block">
						<?php 
				          $mainMenu = array(
				          	'theme_location'    => 'bottom-nav',
				          	'depth'             => 2,
				          	'container'         => 'div',
				          	'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
				          );
				          wp_nav_menu($mainMenu);
				        ?>
				     </div>
				     <div class="footer-social ml-4">
				        <?php echo do_shortcode("[lg-social-media]") ?>
				     </div>
				</div>
				
			</div>
		</div>

		<?php if(!$lg_option_footer_site_legal || $lg_option_footer_site_legal == 'enable'): ?>
			<div id="site-legal" class="py-3 px-3 d-flex justify-content-center justify-content-md-between align-items-center flex-wrap">
				<div class="site-info"><?php get_template_part("/templates/template-parts/footer/site-info"); ?></div>
				<div class="site-longevity"> <?php get_template_part("/templates/template-parts/footer/site-footer-longevity"); ?> </div>
			</div>
		<?php endif; ?>

	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
