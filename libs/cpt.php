<?php

    function lg_custom_post_type(){
      register_post_type( 'product',
          array(
            'labels' => array(
              'name' => __( 'Products' ),
              'singular_name' => __( 'Product' )
            ),
            'public' => true,
            'has_archive' => false,
            'menu_icon'   => 'dashicons-location',
            'show_in_menu'    => 'lg_menu',
            'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
          )
      );
    }

    add_action( 'init', 'lg_custom_post_type' );

    function lg_custom_taxonomy(){

        register_taxonomy(
          'product-category',
          'product',
          array(
            'label' => __( 'Category' ),
            'rewrite' => array( 'slug' => 'product-category' ),
            'hierarchical' => true,
          )
        );
    }

    add_action( 'init', 'lg_custom_taxonomy' );

?>