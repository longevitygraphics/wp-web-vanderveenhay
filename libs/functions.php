<?php

function main_nav_items ( $items, $args ) {
    if ($args->menu->slug == 'main-nav') {
        $items .= '<li class="menu-item menu-item-type-custom menu-item-object-custom nav-item ml-3"><a class="nav-link btn btn-primary text-white" href="tel:+1'.do_shortcode('[lg-phone-main]').'"><span  itemprop="telephone">'.format_phone(do_shortcode('[lg-phone-main]')).'</a></span>';
    }
    return $items;
}
add_filter( 'wp_nav_menu_items', 'main_nav_items', 10, 2 );

add_action('wp_content_top', 'featured_banner_top', 1); // ('wp_content_top', defined function name, order)


    function featured_banner_top(){
        ob_start();
        get_template_part('/templates/template-parts/page/top-banner');
        echo ob_get_clean();
    }

?>