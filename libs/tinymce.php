<?php

	global $lg_tinymce_custom;
	
	$lg_tinymce_custom = array(
	    'title' => 'Custom',
	    'items' =>  array(
	    	array(
				'title' => 'Hay Title',
	            'selector' => 'h1, h2, h3, h4, h5, h6',
	            'classes' => 'hay-title'
			),
			array(
				'title' => 'Underline',
	            'selector' => 'a',
	            'classes' => 'underline'
			)
	    )
	);

?>